package cn.sjtu.lawcase.assignment.rule;
import java.util.ArrayList;
import java.util.List;

import cn.sjtu.lawcase.assignment.data.Judge;

public class CaseNumOfJudgeEqualRule extends Rule{
	public CaseNumOfJudgeEqualRule(List<Judge> j) {
		judges = j;
	}
	
	public CaseNumOfJudgeEqualRule(Judge j1, Judge j2) {
		judges = new ArrayList<>();
		judges.add(j1);
		judges.add(j2);
	}
	
	public String getType() {
		return "CaseNumOfJudgeEqualRule";
	}
}