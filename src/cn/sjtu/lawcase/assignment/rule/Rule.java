package cn.sjtu.lawcase.assignment.rule;
import java.util.ArrayList;
import java.util.List;

import cn.sjtu.lawcase.assignment.data.Case;
import cn.sjtu.lawcase.assignment.data.Judge;


public abstract class Rule{
	public List<Judge> judges = new ArrayList<>();
	public List<Case> cases = new ArrayList<>();
	public List<String> Type = new ArrayList<>();
	public List<Integer> lbs = new ArrayList<>();
	public List<Integer> ubs = new ArrayList<>();
	public abstract String getType();
}









/***
public class Rule{
	public Map<Vector<Judge>,Vector<Case>> c1 = new HashMap<>();
	public Map<Judge,List<Integer>> c2 = new HashMap<>();
	public List<List<Judge>> c3 = new ArrayList<>();
	public Map<Map<Judge,String>,List<Integer>> c4 = new HashMap<>();
	public int typeConstraint;
	public Rule(Judge j, Case c,boolean type) {
		Vector<Judge>J = new Vector<>();
		Vector<Case> C = new Vector<>();
		J.add(j);
		C.add(c);
		c1.put(J, C);
		if (type == true) {
			typeConstraint = 0;
		}
		else {
			typeConstraint = 4;
		}
	}
	

	public Rule(Vector<Judge> j, Case c,boolean type) {
		Vector<Case> C = new Vector<>();
		C.add(c);
		c1.put(j, C);
		if (type == true)
			typeConstraint = 0;
		else
			typeConstraint = 4;
	}
	public Rule(Judge j, Vector<Case> c,boolean type) {
		Vector<Judge> J = new Vector<>();
		J.add(j);
		c1.put(J, c);
		if (type == true)
			typeConstraint = 0;
		else
			typeConstraint = 4;
	}

	public Rule(Vector<Judge> js, Vector<Case> cs,boolean type) {
		c1.put(js, cs);
		if (type == true)
			typeConstraint = 0;
		else
			typeConstraint = 4;
	}
	

	public Rule(Map<Vector<Judge>,Vector<Case>> c,boolean type) {
		c1 = c;
		if (type == true)
			typeConstraint = 0;
		else
			typeConstraint = 4;
	}
	

	public Rule(List<Vector<Judge>> j, List<Vector<Case>> c,boolean type) {
		for (int i=0;i < j.size();++i) {
			c1.put(j.get(i),c.get(i));
		}
		if (type == true)
			typeConstraint = 0;
		else
			typeConstraint = 4;
	}
	

	public Rule(Judge j, int n,String ulc) {
		List<Integer> L = new ArrayList<>();
		if (ulc == "less") {
			L.add(0);
			L.add(n);
			c2.put(j, L);
			typeConstraint = 1;
		}
		
		if (ulc == "greater") {
			L.add(n);
			L.add(100000);
			c2.put(j, L);
			typeConstraint = 1;
		}
	}
	

	public Rule(Judge j, int l, int u) {
		List<Integer> L = new ArrayList<>();
		L.add(l);
		L.add(u);
		c2.put(j, L);
		typeConstraint = 1;
	}
	

	public Rule(Judge j,List<Integer> c) {
		c2.put(j, c);
		typeConstraint = 1;
	}
	
	public Rule(Judge j1,Judge j2) {
		List<Judge> jj = new ArrayList<>();
		jj.add(j1);
		jj.add(j2);
		c3.add(jj);
		typeConstraint = 2;
	}
	
	public Rule(Judge j,String t,int l, int u) {
		HashMap<Judge,String> js = new HashMap<>();
		js.put(j,t);
		
		List<Integer> li = new ArrayList<>();
		li.add(l);
		li.add(u);
		c4.put(js,li);
		typeConstraint = 3;
	}
	
	public Rule(Judge j,String t,int lu,String type) {
		if (type == "less") {
			HashMap<Judge,String> js = new HashMap<>();
			js.put(j,t);
			
			List<Integer> li = new ArrayList<>();
			li.add(0);
			li.add(lu);
			c4.put(js,li);
		}
		
		if (type == "greater") {
			HashMap<Judge,String> js = new HashMap<>();
			js.put(j,t);
			
			List<Integer> li = new ArrayList<>();
			li.add(lu);
			li.add(1000000);
			c4.put(js,li);
		}
		typeConstraint = 3;
	}


	
	
}
***/