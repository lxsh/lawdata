package cn.sjtu.lawcase.assignment.rule;
import java.util.ArrayList;
import java.util.List;

import cn.sjtu.lawcase.assignment.data.Case;
import cn.sjtu.lawcase.assignment.data.Judge;

public class CaseToJudgeRule extends Rule{
	public CaseToJudgeRule(List<Judge> j,List<Case> c) {
		judges = j;
		cases = c;
	}
	public CaseToJudgeRule(Judge j, List<Case> c){
		cases = c;
		judges = new ArrayList<>();
		judges.add(j);
	}
	
	public CaseToJudgeRule(List<Judge> j, Case c) {
		cases = new ArrayList<>();
		cases.add(c);
		judges = j;
	}
	
	public CaseToJudgeRule(Judge j, Case c) {
		cases = new ArrayList<>();
		judges = new ArrayList<>();
		cases.add(c);
		judges.add(j);
	}
	
	public String getType() {
		return "CaseToJudgeRule";
	}
}