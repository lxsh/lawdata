package cn.sjtu.lawcase.assignment.rule;
import java.util.ArrayList;
import java.util.List;

import cn.sjtu.lawcase.assignment.data.Judge;

public class JudgeCaseNumberLimitRule extends Rule{
	public JudgeCaseNumberLimitRule(Judge j,int lb,int ub) {
		judges = new ArrayList<>();
		lbs = new ArrayList<>();
		ubs = new ArrayList<>();
		judges.add(j);
		lbs.add(lb);
		ubs.add(ub);
	}
	
	public JudgeCaseNumberLimitRule(List<Judge> judges, List<Integer> lb,List<Integer>ub) {
		judges = judges;
		lbs = lb;
		ubs = ub;
	}
	
	public JudgeCaseNumberLimitRule(Judge judge, int lub,String s) {
		judges = new ArrayList<>();
		if (s == "MoreThan") {
			lbs.add(lub);
			ubs.add(1000000);
			judges.add(judge);
		}
		
		if(s == "LessThan") {
			lbs.add(0);
			ubs.add(lub);
			judges.add(judge);
		}
	}
	
	public String getType() {
		return "JudgeCaseNumberLimitRule";
	}
}