package cn.sjtu.lawcase.assignment.rule;
import java.util.ArrayList;
import java.util.List;

import cn.sjtu.lawcase.assignment.data.Judge;

public class JudgeTypeOfCasesRule extends Rule{
	public JudgeTypeOfCasesRule(List<Judge> j,List<String> t, List<Integer> lb, List<Integer> ub) {
		judges = j;
		Type   = t;
		lbs    = lb;
		ubs    = ub;
	}
	public JudgeTypeOfCasesRule(Judge judge, List<String> type, List<Integer> lb, List<Integer> ub){
		judges = new ArrayList<>();
		for (String s:type) {
			judges.add(judge);
		}
		Type   = type;
		lbs    = lb;
		ubs    = ub;
	}
	
	public JudgeTypeOfCasesRule(Judge judge, String type, int lb, int ub) {
		lbs = new ArrayList<>();
		ubs = new ArrayList<>();
		Type = new ArrayList<>();
		judges = new ArrayList<>();
		judges.add(judge);
		Type.add(type);
		lbs.add(lb);
		ubs.add(ub);
	}

	public JudgeTypeOfCasesRule(Judge judge, String type, int lub, String typ) {
		lbs = new ArrayList<>();
		ubs = new ArrayList<>();
		Type = new ArrayList<>();
		judges = new ArrayList<>();
		judges.add(judge);
		Type.add(type);
		if (typ=="MoreThan") {
			lbs.add(lub);
			ubs.add(10000000);
		}
		if(typ=="LessThan") {
			lbs.add(0);
			ubs.add(lub);
		}
	}
	
	public String getType() {
		return "JudgeTypeOfCasesRule";
	}
}