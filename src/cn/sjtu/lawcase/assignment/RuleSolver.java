package cn.sjtu.lawcase.assignment;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Vector;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.ortools.constraintsolver.Constraint;
import com.google.ortools.constraintsolver.DecisionBuilder;
import com.google.ortools.constraintsolver.IntVar;
import com.google.ortools.constraintsolver.Solver;
import com.google.ortools.sat.LinearExpr;

import cn.sjtu.lawcase.assignment.data.Case;
import cn.sjtu.lawcase.assignment.data.Judge;
import cn.sjtu.lawcase.assignment.rule.CaseNotToJudgeRule;
import cn.sjtu.lawcase.assignment.rule.CaseNumOfJudgeEqualRule;
import cn.sjtu.lawcase.assignment.rule.CaseToJudgeRule;
import cn.sjtu.lawcase.assignment.rule.JudgeCaseNumberLimitRule;
import cn.sjtu.lawcase.assignment.rule.JudgeTypeOfCasesRule;
import cn.sjtu.lawcase.assignment.rule.Rule;

public class RuleSolver{

	static {
		System.loadLibrary("jniortools");
	}
	private IntVar [][] assigns;
	private IntVar [] assignFlat;
	private HashMap<Case,Integer> CaseInd = new HashMap<>();
	private HashMap<Judge,Integer> JudgeInd = new HashMap<>();
	private HashMap<Integer,Case> RCInd = new HashMap<>();
	private HashMap<Integer,Judge> RJInd = new HashMap<>(); 
	private HashMap<String,List<Integer>> CaseType = new HashMap<>();
	private List<Rule> ruleSet = new ArrayList<>();
	private int judgesize;
	private int casesize;
	private DecisionBuilder db;
	private Solver solver = new Solver("solver");
	private List<long[][]> solutions = new ArrayList<>();
	private String conflicts = "";
	
	
	private List<Judge> roadJudgeFromExcel(String filename) {
		List<Judge> js = new ArrayList<>();
		File file = new File(filename);
		Workbook wb = null;
		if(!file.exists()) {
			System.out.println("文件不存在");
			return js;
		}
		String fileType = filename.substring(filename.lastIndexOf("."));
		try {
			InputStream is = new FileInputStream(filename);
			if (".xls".equals(fileType)) {
				wb = new HSSFWorkbook(is);
			}else if(".xlsx".equals(fileType)) {
				wb = new XSSFWorkbook(is);
			}
			else {
				System.out.println("格式不可读取");
				return js;
			}
		}catch (Exception e) {
			e.printStackTrace();
			return js;
		}
		Sheet sheet = wb.getSheetAt(0);
		int rowNum = sheet.getLastRowNum();
		for (int i=1;i<=rowNum;++i) {
			Row row = sheet.getRow(i);
			int colNum = row.getLastCellNum();
			for (int j=0;j<colNum;++j) {
				Cell cell = row.getCell(j);
				if (cell==null)
					continue;
			}
			Cell namecell = row.getCell(0);
			Cell agecell = row.getCell(1);
			Cell gendercell = row.getCell(2);
			Judge tmp = new Judge(namecell.toString(),gendercell.toString(),Integer.parseInt(agecell.toString()));
			js.add(tmp);
		}
		return js;
	}
	private List<Case> roadCaseFromExcel(String filename) {
		List<Case> js = new ArrayList<>();
		File file = new File(filename);
		Workbook wb = null;
		if(!file.exists()) {
			System.out.println("文件不存在");
			return js;
		}
		String fileType = filename.substring(filename.lastIndexOf("."));
		try {
			InputStream is = new FileInputStream(filename);
			if (".xls".equals(fileType)) {
				wb = new HSSFWorkbook(is);
			}else if(".xlsx".equals(fileType)) {
				wb = new XSSFWorkbook(is);
			}
			else {
				System.out.println("格式不可读取");
				return js;
			}
		}catch (Exception e) {
			e.printStackTrace();
			return js;
		}
		Sheet sheet = wb.getSheetAt(0);
		int rowNum = sheet.getLastRowNum();
		for (int i=1;i<=rowNum;++i) {
			Row row = sheet.getRow(i);
			int colNum = row.getLastCellNum();
			for (int j=0;j<colNum;++j) {
				Cell cell = row.getCell(j);
				if (cell==null)
					continue;
			}
			Cell namecell = row.getCell(0);
			Cell agecell = row.getCell(1);
			Cell gendercell = row.getCell(2);
			Case tmp = new Case(namecell.toString(),gendercell.toString(), agecell.toString());
			js.add(tmp);
		}
		return js;
	}
	
	public RuleSolver(String caseFile, String judgeFile) {
		List<Case> cs = roadCaseFromExcel(caseFile);
		List<Judge> js = roadJudgeFromExcel(judgeFile);
		this.init(cs, js);
	}
	
	public RuleSolver(String url,String user,String pswd) throws ClassNotFoundException, SQLException{
	    final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	    Class.forName(JDBC_DRIVER);
	    List<Judge> jl = new ArrayList<>();
	    List<Case> cl = new ArrayList<>();
	    Connection conn = DriverManager.getConnection(url+"?serverTimezone=UTC&characterEncoding=utf-8",user,pswd);
	    if(!conn.isClosed()) {
	    	Statement st = conn.createStatement();
	    	String sql1 = "select * from judges";
	    	ResultSet rs = st.executeQuery(sql1);
	    	while(rs.next()) {
	    		Judge j = new Judge(rs.getString("Name"),rs.getString("Gender"),rs.getInt("Age"));
	    		jl.add(j);
	    	}
	    	rs.close();
	
	    	String sql2 = "select * from cases";
	    	rs = st.executeQuery(sql2);
	    	while(rs.next()){
	    		Case c = new Case(rs.getString("Name"),rs.getString("Type"),rs.getString("Time"));
	    		cl.add(c);
	    	}
	    	rs.close();
	    	conn.close();
	    }
	    init(cl,jl);
	}
	private void init(List<Case> cases, List<Judge> judges) {
		int y = cases.size();
		int x = judges.size();
		this.assigns = new IntVar [x][y];
		int [] diff = new int [y];
		int [] abb  = new int [x];
		this.assignFlat = new IntVar[x*y];
		for(int i =0;i<x;++i) {
			Judge jd = judges.get(i);
			abb[i] = jd.ability;
			this.JudgeInd.put(jd,i);
			this.RJInd.put(i, judges.get(i));
			for(int j=0;j<y;++j) {
				Case c = cases.get(j);
				diff[j] = c.difficulty;
				this.CaseInd.put(c, j);
				this.RCInd.put(j,cases.get(j));
				this.assigns[i][j] = this.solver.makeIntVar(0,1,"x"+i+"_"+j);
				this.assignFlat[i*y+j] = this.assigns[i][j];
			}
		}
		
		for (Case c: cases) {
			String type = c.type;
			if (this.CaseType.containsKey(type)) {
				List<Integer> l = this.CaseType.get(type);
				l.add(this.CaseInd.get(c));
				CaseType.put(type, l);
			}
			else {
				List<Integer> l = new ArrayList<>();
				l.add(this.CaseInd.get(c));
				CaseType.put(type, l);
			}
		}
		
		for (int j=0;j<y;++j) {
			IntVar [] row = new IntVar[x];
			for (int i=0;i<x;++i) {
				row[i] = assigns[i][j];
			}
			
			this.solver.addConstraint(this.solver.makeSumEquality(row, 1));
		}
		for (int i=0;i<x;++i) {
			IntVar [] row = new IntVar[y];
			for (int j=0;j<y;++j) {
				row[j] = assigns[i][j];
			}
			
			this.solver.makeScalProdLessOrEqual(row, diff, abb[i]);
		}
		//System.out.println(assigns[0][0]);
		this.judgesize = x;
		this.casesize  = y;
		db = solver.makePhase(this.assignFlat,this.solver.CHOOSE_FIRST_UNBOUND, this.solver.ASSIGN_MIN_VALUE);
		
	}
	
	public RuleSolver(List<Case> cases, List<Judge> judges) {
		init(cases,judges);
		System.out.println(this.judgesize);
		System.out.println(this.casesize);
	}
	
	public void addRule(Rule con) {
		int l = con.judges.size();
		for (Case c:con.cases) {
			IntVar [] row = new IntVar [l];
			for (Judge j:con.judges) {

				for (int i=0;i<l;++i) {
					row[i] = assigns[JudgeInd.get(j)][CaseInd.get(c)];
				}
				Constraint cc= this.solver.makeSumEquality(row, 1);
				if (this.solver.checkConstraint(cc)) {
					ruleSet.add(con);
					this.solver.addConstraint(cc);;
				}
				else {
					this.conflicts += "必须将案件\t"+c.name+"交给法官\t";
					for (Judge jj:con.judges) {
						this.conflicts += jj.name +",";
					}
					this.conflicts += "审理";
				}
			}
		}
	};
	
	public void addRule(CaseToJudgeRule con) {
		int l = con.judges.size();
		for (Case c:con.cases) {
			IntVar [] row = new IntVar [l];
			for (Judge j:con.judges) {

				for (int i=0;i<l;++i) {
					row[i] = assigns[JudgeInd.get(j)][CaseInd.get(c)];
				}
				Constraint cc= this.solver.makeSumEquality(row, 1);
				if (this.solver.checkConstraint(cc)) {
					Rule tmp = new CaseToJudgeRule(con.judges,c);
					ruleSet.add(tmp);
					this.solver.addConstraint(cc);
				}
				else {
					this.conflicts += "必须将案件\t"+c.name+"交给法官\t";
					for (Judge jj:con.judges) {
						this.conflicts += jj.name +",";
					}
					this.conflicts += "审理\n";
				}
			}
		}

	}
	
	public void addRule(CaseNotToJudgeRule con) {
		int l = con.judges.size();
		for (Case c:con.cases) {
			IntVar [] row = new IntVar [l];
			for (Judge j:con.judges) {

				for (int i=0;i<l;++i) {
					row[i] = assigns[JudgeInd.get(j)][CaseInd.get(c)];
				}
				Constraint cc= this.solver.makeSumEquality(row, 0);
				if (this.solver.checkConstraint(cc)) {
					Rule tmp = new CaseNotToJudgeRule(con.judges,c);
					ruleSet.add(tmp);
					this.solver.addConstraint(cc);;
				}
				else {
					this.conflicts += "必须不能将案件\t"+c.name+"交给法官\t";
					for (Judge jj:con.judges) {
						this.conflicts += jj.name +",";
					}
					this.conflicts += "审理\n";
				}
			}
		}

	}
	
	public void addRule(CaseNumOfJudgeEqualRule con) {
		Judge j1 = con.judges.get(0);
		List<Judge> tmp = new ArrayList<>();
		tmp.add(j1);
		for (Judge j2: con.judges.subList(1, con.judges.size())) {
			Constraint cc = this.solver.makeEquality(this.solver.makeSum(assigns[JudgeInd.get(j1)]), this.solver.makeSum(assigns[JudgeInd.get(j2)]));
			if (this.solver.checkConstraint(cc)) {
				tmp.add(j2);
				this.solver.addConstraint(cc);
			}
			else {
				this.conflicts += "法官\t" + j1.name +"和法官\t" + j2.name +"的审理案件数必须相等\n";
			}
		}
		CaseNumOfJudgeEqualRule rtmp = new CaseNumOfJudgeEqualRule(tmp);
		this.ruleSet.add(rtmp);
	}
	
	public void addRule(JudgeCaseNumberLimitRule con) {
		for (int i =0; i<con.judges.size();++i) {
			int jint = JudgeInd.get(con.judges.get(i));
			Constraint cc1 = this.solver.makeSumGreaterOrEqual(assigns[jint], con.lbs.get(i));
			Constraint cc2 = this.solver.makeSumLessOrEqual(assigns[jint], con.ubs.get(i));
			if (this.solver.checkConstraint(cc1) &&this.solver.checkConstraint(cc2)) {
				JudgeCaseNumberLimitRule tmp = new JudgeCaseNumberLimitRule(con.judges.get(i),con.lbs.get(i),con.ubs.get(i));
				ruleSet.add(tmp);
				this.solver.addConstraint(cc1);
				this.solver.addConstraint(cc2);
			}
			else 
				this.conflicts += "法官\t" + con.judges.get(i).name + "审理案件数区间:" + con.lbs.get(i) +"," + con.ubs.get(i) +"\n"; 
		}
	}
	
	public void  addRule(JudgeTypeOfCasesRule con) {
		for (int i = 0;i < con.judges.size();++i) {
			try {
				List<Integer> cases = CaseType.get(con.Type.get(i));
				IntVar [] row = new IntVar[cases.size()];
				for(int j = 0;j<cases.size();++j) {
					ruleSet.add(con);
					row[j] = this.assigns[JudgeInd.get(con.judges.get(i))][cases.get(j)];
				}
				Constraint cc1 = this.solver.makeSumGreaterOrEqual(row, con.lbs.get(i));
				Constraint cc2 = this.solver.makeSumLessOrEqual(row, con.ubs.get(i));
				if (this.solver.checkConstraint(cc1) && this.solver.checkConstraint(cc2)) {
					JudgeTypeOfCasesRule tmp = new JudgeTypeOfCasesRule(con.judges.get(i),con.Type.get(i),con.lbs.get(i),con.ubs.get(i));
					ruleSet.add(tmp);
					this.solver.addConstraint(cc1);
					this.solver.addConstraint(cc2);
				}
				else {
					this.conflicts += "法官\t" + con.judges.get(i).name +"审理类型为" + con.Type.get(i) +"的案件数区间" + con.lbs.get(i) +"," + con.ubs.get(i) +"\n";
				}
			}
			catch(Exception e){
				System.out.println(e);
				System.out.println("案件类型不存在");}		
	
		}

	}

	
/*
	public void addRule(Rule con) {
		if (con.typeConstraint == 0) {
			//System.out.println(this.model.modelStats());
			addRule1(con);
		}
		
		if (con.typeConstraint == 1) {
			//System.out.println(this.model.modelStats());
			addRule2(con);
		}
		
		if (con.typeConstraint == 2) {
			addRule3(con);
		}
		
		if (con.typeConstraint == 3) {
			addRule4(con);
		}
		
		if(con.typeConstraint == 4) {
			addRule5(con);
		}

	}
	
	public void addRules(List<Rule> con) {
		for (Rule c:con) {
			addRule(c);
		}
	}
	
	private void addRule1(Rule con) {
		Iterator<Map.Entry<Vector<Judge>, Vector<Case>>> entries = con.c1.entrySet().iterator();
		while(entries.hasNext()){
			Map.Entry<Vector<Judge>, Vector<Case>> jc = entries.next();
			Vector<Judge> js = jc.getKey();
			Vector<Case>  cs = jc.getValue();
			int l = js.size();
			int [] ind = new int [l];
			int cnt = 0;
			
			Vector<Judge> backupjudge = new Vector<>();
			for (Judge j: js) {
				int id  = JudgeInd.get(j);
				ind[cnt] = id;
				cnt ++;
				backupjudge.add(j);
			}
			for (Case c: cs) {
				int indc = CaseInd.get(c);
				//System.out.println(indc);
				IntVar [] row = new IntVar [js.size()];
				for (int i=0;i<cnt;++i) {
					row[i] = assigns[ind[i]][indc];
				}
				Constraint cc = this.solver.makeSumEquality(row, 1);
				if (this.solver.checkConstraint(cc))
					this.solver.addConstraint(cc);
				else {
					this.conflicts += "Must Assigned among Judges:\t";
					for (Judge j:js) {
						this.conflicts += j.name +",";
					}
					this.conflicts += "Cases:\t" + c.name+"\n";
				}
			}
		}
	}
	
	private void addRule2(Rule con) {
		Iterator<Entry<Judge, List<Integer>>> entries = con.c2.entrySet().iterator();
		while(entries.hasNext()) {
			Map.Entry<Judge,List<Integer>> jc = entries.next();
			int lb = jc.getValue().get(0);
			int ub = jc.getValue().get(1);
			Judge j = jc.getKey();
			Constraint cc1 = this.solver.makeSumGreaterOrEqual(assigns[JudgeInd.get(j)], lb);
			Constraint cc2 = this.solver.makeSumLessOrEqual(assigns[JudgeInd.get(j)], ub);
			if (this.solver.checkConstraint(cc1) &&this.solver.checkConstraint(cc2)) {
				this.solver.addConstraint(cc1);
				this.solver.addConstraint(cc2);
			}
			else {
				this.conflicts += "Judge:" + j.name + "bounds:" + lb +"," + ub +"\n"; 
			}
		}
	}
	
	private void addRule3(Rule con) {
		for(List<Judge> js : con.c3) {
			Judge j1 = js.get(0);
			for (Judge j2: js) {
				Constraint cc = this.solver.makeEquality(this.solver.makeSum(assigns[JudgeInd.get(j1)]), this.solver.makeSum(assigns[JudgeInd.get(j2)]));
				if (this.solver.checkConstraint(cc)) {
					this.solver.addConstraint(cc);
				}
				else {
					this.conflicts += "Identical number of cases \t Judges:\t" + j1.name +"," + j2.name +"\n";
				}
			}
		}
	}
	
	private void addRule4(Rule con) {
		Iterator<Entry<Map<Judge, String>, List<Integer>>> entries = con.c4.entrySet().iterator();
		while(entries.hasNext()) {
			Map.Entry<Map<Judge, String>, List<Integer>> jc = entries.next();
			Map<Judge,String> jcs = jc.getKey();
			Iterator<Entry<Judge, String>> ji = jcs.entrySet().iterator();
			List<Integer> lub = jc.getValue();
			int lb = lub.get(0);
			int ub = lub.get(1);
			while(ji.hasNext()) {
				Map.Entry<Judge, String> tmpjs = ji.next();
				int tmpj = JudgeInd.get(tmpjs.getKey());
				try {
					List<Integer> cases = CaseType.get(tmpjs.getValue());
					IntVar [] row = new IntVar[cases.size()];
					for(int i = 0;i<cases.size();++i) 
						row[i] = this.assigns[tmpj][cases.get(i)];
					Constraint cc1 = this.solver.makeSumGreaterOrEqual(row, lb);
					Constraint cc2 = this.solver.makeSumLessOrEqual(row, ub);
					if (this.solver.checkConstraint(cc1) && this.solver.checkConstraint(cc2)) {
						this.solver.addConstraint(cc1);
						this.solver.addConstraint(cc2);
					}
					else {
						this.conflicts += "Judges:\t" + tmpjs.getKey().name +"," + "type:" + tmpjs.getValue() +"," + lb +"," + ub +"\n";
					}
				}
				catch(Exception e){
					System.out.println("Type not exist");}
				
				}			
			}
	}
	
	private void addRule5(Rule con) {
		Iterator<Map.Entry<Vector<Judge>, Vector<Case>>> entries = con.c1.entrySet().iterator();
		while(entries.hasNext()){
			Map.Entry<Vector<Judge>, Vector<Case>> jc = entries.next();
			Vector<Judge> js = jc.getKey();
			Vector<Case>  cs = jc.getValue();
			int l = js.size();
			int [] ind = new int [l];
			int cnt = 0;
			
			Vector<Judge> backupjudge = new Vector<>();
			for (Judge j: js) {
				int id  = JudgeInd.get(j);
				ind[cnt] = id;
				cnt ++;
				backupjudge.add(j);
			}
			for (Case c: cs) {
				int indc = CaseInd.get(c);
				//System.out.println(indc);
				IntVar [] row = new IntVar [js.size()];
				for (int i=0;i<cnt;++i) {
					row[i] = assigns[ind[i]][indc];
				}
				Constraint cc= this.solver.makeSumEquality(row, 0);
				if (this.solver.checkConstraint(cc)) {
					this.solver.addConstraint(cc);;
				}
				else {
					this.conflicts += "Must not Assigned to Judges:\t";
					for (Judge j:js) {
						this.conflicts += j.name +",";
					}
					this.conflicts += "Cases:\t" + c.name+"\n";
				}
			}
		}
	}
*/
	
	/**
	 *<p>This function is used to solve and find results. the variable is the number of solutions fetched back </p>
	 *@param times number of results required
	 */
	public  void solve(int times) {
		int sol = 0;
		solver.makeSolutionsLimit(5);
		solver.newSearch(db);
		while (solver.nextSolution()) {
			sol ++;
			if (sol > times)
				break;
			long [][] solution = new long[this.judgesize][this.casesize];
			for(int i=0;i<this.judgesize;++i)
				for (int j=0;j<this.casesize;++j)
					solution[i][j] = this.assigns[i][j].value();
			this.solutions.add(solution);
		}
	}

	/**
	 * @return ${After calling solve function, this function is used to fetch results. The returned object is a map, with (solution,objective) format solution is in the format of (judge, List[Case] ), represents the cases assigned to the judge.}$
	 */
	
	public String conflicts() {
		return this.conflicts;
	}
	
	
	public HashMap<HashMap<Judge,List<Case>>,Integer> result(){

		HashMap<HashMap<Judge,List<Case>>,Integer> res = new HashMap<>();
		for (long[][] s:this.solutions) {
			//System.out.println(s);
			int max = 0;
			int min = 100000000;
			HashMap<Judge,List<Case>> solve = new HashMap<>();
			for (int i=0;i<this.RJInd.size();++i) {
				Judge judge = RJInd.get(i);
				List<Case> l = new ArrayList<>();
				for (int j=0;j<this.RCInd.size();++j) {
					if (s[i][j]==1) {
						l.add(RCInd.get(j));
					}
					max = Math.max(max, l.size());
					min = Math.min(min,l.size());
				}
				solve.put(judge,l);
			}
			int v = max-min;
			res.put(solve, v);
		}
		//System.out.println(res.size());
		return res;
	}
	
	public void printInfo() {
		System.out.println("已添加法官信息如下:\n");
		for(Judge j: this.JudgeInd.keySet()) {
			System.out.print(j.name+"\t");
		}
		
		System.out.println("\n已添加案件信息如下:\n");
		for(Case c: this.CaseInd.keySet()) {
			System.out.print(c.name+"\t");
		}
		
		System.out.println("\n已添加的分案限制如下:\n");
		for(Rule r: this.ruleSet) {
			String s = r.getType();
			switch(s) {
			case "CaseToJudgeRule":
				System.out.print("案件 ");
				System.out.print(r.cases.size());
				for (Case c:r.cases)
					System.out.print(c.name+",");
				System.out.print("必须由法官 ");
				for (Judge j:r.judges)
					System.out.print(j.name+",");
				System.out.print("审理");
				break;
			case "CaseNotToJudgeRule":
				System.out.print("案件 ");
				for (Case c:r.cases)
					System.out.print(c.name+",");
				System.out.print("必须不能由法官 ");
				for (Judge j:r.judges)
					System.out.print(j.name+",");
				System.out.print("审理");
				break;				
			case "CaseNumOfJudgeEqualRule":
				System.out.print("法官");
				for (Judge j:r.judges)
					System.out.print(j.name+",");
				System.out.print("必须有相同的案件数需要审理");
				break;	
			case "JudgeTypeOfCasesRule":
				for (int i=0;i<r.Type.size();++i) {
					System.out.print("法官 ");
					System.out.print(r.judges.get(i).name+",");
					System.out.print("审理类型为");
					System.out.print(r.Type.get(i)+"的案件数量为");
					System.out.print(r.lbs.get(i)+"至"+r.ubs.get(i)+"件");
				}
				break;	
			case "JudgeCaseNumberLimitRule":
				for (int i=0;i<r.Type.size();++i) {
					System.out.print("法官 ");
					System.out.print(r.judges.get(i).name+",");
					System.out.print("审理的案件数量为");
					System.out.print(r.lbs.get(i)+"至"+r.ubs.get(i)+"件");
				}
				break;				
			}
			System.out.println();
		}
	}
	public void printResults() {
		HashMap<HashMap<Judge,List<Case>>,Integer> r = this.result();
		int i = 1;
		for (HashMap<Judge,List<Case>> k:r.keySet()) {
			System.out.println("方案"+i +":\n");
			System.out.println("-------------------------------------------------------------------------");
			for (Judge sk: k.keySet()) {
				System.out.println();
				System.out.println("法官:\t"+sk.name+"\n");
				System.out.print("案件:\n");
				for(Case ck: k.get(sk)) {
					System.out.print(ck.name);
					System.out.print(",");
				}
				System.out.println();
				System.out.println();
			}
			i ++;
			System.out.println();
			System.out.println("-------------------------------------------------------------------------");
		}
		
	}
	public void dumpTo(String file) throws IOException {
		File f = new File(file);
		if (!f.exists()) {
			f.createNewFile();
		}
		FileWriter fw = new FileWriter(f);
		BufferedWriter bw = new BufferedWriter(fw);
		String toWrite = "";
		for(long [][]s:this.solutions) {
			for (int i=0;i<this.RJInd.size();++i) {
				Judge judge = RJInd.get(i);
				toWrite += "Judge: " + judge.name + ", Age: " + judge.age + ", Gender: " + judge.gender +"\t";
				for(int j=0;j<this.RCInd.size();++j) {
					if (s[i][j]== 1) {
						Case tc = RCInd.get(j);
						toWrite += "Cases: " + "Name: " + tc.name + ",type:" +tc.type +"\t"; 
					}
				}
				toWrite += "\n";
			}
			toWrite += "---------------------------------------------\n";
			bw.write(toWrite);
			toWrite = "";
		}
		bw.close();
		
	}
	/*
	public RuleSolver(String file1,String file2) throws FileNotFoundException {
		List<Judge> jl = new ArrayList<>();
		List<Case> cl = new ArrayList<>();
		File f = new File(file1);
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		try {
			String line = br.readLine();
			while(line != null) {
				String [] u = line.substring(0,line.length()-1).split("\t");
				for (String s:u) {
					String [] su = s.split(",");
					Judge j = new Judge(su[0],su[1],Integer.parseInt(su[2]));
					jl.add(j);
				}
				line = br.readLine();
			br.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		File ff = new File(file2);
		FileReader frr = new FileReader(ff);
		BufferedReader brr = new BufferedReader(frr);
		try {
			String line = brr.readLine();
			while(line != null) {
				String [] u = line.substring(0,line.length()-1).split("\t");
				for (String s:u) {
					String [] su = s.split(",");
					Case c = new Case(su[0],su[1],su[2]);
					cl.add(c);
				}
				line = brr.readLine();
			brr.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		init(cl,jl);
	}
	*/
	public static String getRandomString(int length){
	     String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	     Random random=new Random();
	     StringBuffer sb=new StringBuffer();
	     for(int i=0;i<length;i++){
	       int number=random.nextInt(62);
	       sb.append(str.charAt(number));
	     }
	     return sb.toString();
	 }
	 
	public static void main(String[] args) {
		
		List<Case> c = new ArrayList<>();
		for(int i =0;i<100;++i) {
			String name = getRandomString(10);
			String type = getRandomString(10);
			Case a = new Case(name,type);
			c.add(a);
		}
		List<Judge> j = new ArrayList<>();
		for(int i=0;i<50;++i) {
			String name = getRandomString(10);
			Judge aa = new Judge(name);
			j.add(aa);
		}
		
		RuleSolver t = new RuleSolver(c,j);

		System.out.println("init done");
		for(int i=0;i<5;++i) {
			Random random = new Random();
			int number1 = random.nextInt(4);
			int number2 = random.nextInt(9);
			System.out.println(c.get(number2).name);
			CaseToJudgeRule con1 = new CaseToJudgeRule(j.get(number1),c.get(number2));
			System.out.println(con1.cases);
			t.addRule(con1);			
		}
		
		t.solve(1);

		t.printInfo();
		t.printResults();
		//System.out.println(r.size());

		return;
		
	}
}