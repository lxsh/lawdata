package cn.sjtu.lawcase.assignment.data;

public class Judge{
	public String name;
	public String gender;
	public int age;
	public int ability;
	
	/**
	 * @param a name
	 * @param b gender
	 * @param c age
	 */
	public Judge(String Name, String Gender, int Age,int Ability) {
		name= Name;
		gender = Gender;
		age = Age;
		ability = Ability;
	}

	
	public Judge(String Name, String Gender, int Age) {
		name= Name;
		gender = Gender;
		age = Age;
		ability = 100000000;
	}
	/**
	 * @param a name
	 * <p> other two elements are sent to empty string and -1 </p>
	 */
	public Judge(String Name) {
		name = Name;
		gender ="";
		age = -1;
		ability = 1000000000;
	}
}