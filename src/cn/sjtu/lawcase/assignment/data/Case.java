package cn.sjtu.lawcase.assignment.data;
public class Case{
	public String name;
	public String time;
	public String type;
	public int difficulty;
	/**
	 * @param a case name
	 * @param b case establish time
	 */
	public Case(String Name, String Type,String Time,int Difficulty) {
		name= Name;
		type = Type;
		time = Time;
		difficulty = Difficulty;
	}	

	public Case(String Name, String Type,String Time) {
		name= Name;
		type = Type;
		time = Time;
		difficulty = 0;
	}	
	/**
	 * @param a case name
	 * <p> time set to empty string</p>
	 */
	public Case(String Name, String Type) {
		name = Name;
		type = Type;
		time = "";
		difficulty = 0;
	}
}