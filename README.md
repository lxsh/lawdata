# 法院分案支持库

---

## 简介

---

该项目利用约束编程(CP)的方式完成法院中对案件的分发。该项目由java完成，且使用了google开源的ortools库作为CP的技术实现库。

整个文件即为项目的源码，最终请使用eclipse导出*jar*包，并附带*lib*目录下的文件以供可移植性使用。

## 制定分案规则

---

请利用 ***cn.sjtu.lawcase.assignment.data.Case/Judge*** 来对案件个体和法官个体进行初始化。同时，也请利用 ***cn.sjtu.lawcase.assignment.rule*** 下的类来进行约束初始化，其中，每个类支持不同的约束初始化。 我们的接口支持调用***RuleSolver(String caseFile, String judgeFile)*** 初始化函数从文件中读取法官信息与案件信息。也支持调用 ***RuleSolver(String url,String user,String pswd)*** 从数据库中读取法官信息与案件信息。 如果不利用以上两种方式读取信息，请使用***RuleSolver(List< Judge >,List< Case >)*** 传入相应的Case类的list/单一元素 与Judge类的list/单一元素 进行solver创建。

***CaseNotToJudgeRule***: 某些案件不能被分配给某些法官

***CaseNumOfJudgeEqualRule***: 某些法官分配到的案件数应当相等

***CaseToJudgeRule***: 案件必须分配给某些法官

***JudgeCaseNumberLimitRule***: 法官的审理案件的数量的阈值

***JudgeTypeOfCasesRule***:法官审理某种类型案件的数量限制

在初始化不同的Rules后，请使用***solver.addRule(Rule rule)***添加相应的限制条件。 调用***solver.printInfo()*** 查看已经添加的限制条件。

## 寻找最优方案

---

请调用***solver.solve(nums)*** 进行求解，nums代表返回解的数量。

求解后，调用 solver.result() 返回 一个HashMap，存储不同分案方法以及对应的最大最小分案数量的差距。 分案方法中存储的是另一个HashMap<Judge,List<Case>>,代表每个法官分配到的案件。

调用***solver.printResults()*** 直接输出分案的结果。

调用***solver.conflicts()*** 查看添加的限制条件是否存在冲突。

其他注意事项
请注意在 RuleSolver.java中，我们提供了一个main函数，随机生成了150名法官与1000个案件以及50个限制条件，并进行求解。