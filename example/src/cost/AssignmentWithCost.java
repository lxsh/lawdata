package cost;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;


import cn.sjtu.lawcase.assignment.RuleSolver;
import cn.sjtu.lawcase.assignment.data.Case;
import cn.sjtu.lawcase.assignment.data.Judge;
import cn.sjtu.lawcase.assignment.rule.CaseToJudgeRule;

public class AssignmentWithCost {

	//考虑案件难度和法官审案能力的例子
	public static void main(String[] args) {
		//对于法官，不赋予能力值，则认为法官的能力可以承载所有案件审理
		//法官a为男性，21岁，能力为5
		Judge a = new Judge("a","男",21,5);
		//法官b为女性，25岁，能力为10
		Judge b = new Judge("b","女",25,10);
		//对于案件，不赋予难度，则认为案件没有难度
		//案件A为离婚案件，须在6-20前审理，难度为3
		Case  A = new Case("A","devoice","2020-06-20",3);
		//案件B为离婚案件，须在6-21前审理，难度为5
		Case  B = new Case("B","devoice","2020-06-21",5);
		//案件C为财产纠纷，无审理期限，无难度
		Case  C = new Case("C","property");
		List<Judge> jl = new ArrayList<>();
		List<Case> cl  = new ArrayList<>();
		jl.add(a);
		jl.add(b);
//		jl.add(c);
		cl.add(A);
		cl.add(B);
		cl.add(C);
		Vector<Case> cs = new Vector<>();
		cs.add(A);
		cs.add(B);
		RuleSolver slv = new RuleSolver(cl,jl);
//		slv.addRule(rule);
        //计算2个可行解并将可行解输出
		//分配结果自动满足案件难度综合不超过法官能力的要求
		slv.solve(2);
		slv.printResults();
		
	}

}
