package scale;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;


import cn.sjtu.lawcase.assignment.RuleSolver;
import cn.sjtu.lawcase.assignment.data.Case;
import cn.sjtu.lawcase.assignment.data.Judge;
import cn.sjtu.lawcase.assignment.rule.CaseToJudgeRule;
import cn.sjtu.lawcase.assignment.rule.Rule;

public class LargeScale{

	public static void main(String[] args) {
		Random r = new Random(1);
		List<Case> c = new ArrayList<>();
		for(int i =0;i<2000;++i) {
			String name = "案件"+i;
			String type = "类型"+r.nextInt(5);
			Case a = new Case(name,type);
			c.add(a);
		}
		List<Judge> j = new ArrayList<>();
		for(int i=0;i<50;++i) {
			String name = "法官"+i;
			Judge aa = new Judge(name);
			j.add(aa);
		}
		
		RuleSolver t = new RuleSolver(c,j);

//		List<Rule> r = new ArrayList<>();
		for(int i=0;i<100;++i) {
			Random random = new Random();
			int number1 = random.nextInt(49);
			int number2 = random.nextInt(999);
			Rule con1 = new CaseToJudgeRule(j.get(number1),c.get(number2));
			t.addRule(con1);			
		}
		System.out.println("init done");
		t.solve(1);
		HashMap<HashMap<Judge,List<Case>>,Integer> rr = new HashMap<>();
		rr = t.result();
		//System.out.println(r.size());
		for (HashMap<Judge,List<Case>> k:rr.keySet()) {
			for (Judge sk: k.keySet()) {
				System.out.println(sk.name);
				System.out.print("[");
				for(Case ck: k.get(sk)) {
					System.out.print(ck.name);
					System.out.print(",");
				}
				System.out.println();
			}
			
		}
		return;
	}
}