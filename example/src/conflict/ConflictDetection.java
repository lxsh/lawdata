package conflict;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import cn.sjtu.lawcase.assignment.RuleSolver;
import cn.sjtu.lawcase.assignment.data.Case;
import cn.sjtu.lawcase.assignment.data.Judge;
import cn.sjtu.lawcase.assignment.rule.CaseToJudgeRule;




public class ConflictDetection{
	public void test(List<Judge> judges, List<Case> cases) {
		//规则需要离婚案件A,B均由法官a审理，同时又需要离婚案件A由法官b审理
		CaseToJudgeRule con1 = new CaseToJudgeRule(judges,cases);
		CaseToJudgeRule con2 = new CaseToJudgeRule(judges,cases);
        RuleSolver slv = new RuleSolver(cl,jl);
        slv.addRule(con1);
        slv.addRule(con2);
        slv.solve(2);
        //基于以上的情况，实际上存在冲突，因此根据我们的设计，后一个输入的限制条件即离婚案件A由法官b审理，将会作为冲突输出到confilicts中
        System.out.print(slv.conflicts());		
	}
	public static void main(String[] args) {
		Judge a = new Judge("a");
		Judge b = new Judge("b");
		Judge c = new Judge("c");
		Case  devoiceA = new Case("A","devoice");
		Case  devoiceB = new Case("B","devoice");
		Case  propertyC = new Case("C","property");
		List<Judge> jl = new ArrayList<>();
		List<Case> cl  = new ArrayList<>();
		jl.add(a);
		jl.add(b);
		jl.add(c);
		cl.add(devoiceA);
		cl.add(devoiceB);
		cl.add(propertyC);
		
		ConflictDetection cfd = new ConflictDetection();
		cfd.test(jl,cl);

	}
}