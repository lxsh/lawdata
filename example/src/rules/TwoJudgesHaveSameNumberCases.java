package rules;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import cn.sjtu.lawcase.assignment.RuleSolver;
import cn.sjtu.lawcase.assignment.data.Case;
import cn.sjtu.lawcase.assignment.data.Judge;
import cn.sjtu.lawcase.assignment.rule.CaseNumOfJudgeEqualRule;
public class TwoJudgesHaveSameNumberCases{
	//添加限制条件：法官a,b审理的案件数量必须相同，并对添加限制条件后的案件进行分案，并输出结果
	public static void main(String[] args) {
		Judge a = new Judge("a");
		Judge b = new Judge("b");
		Case  A = new Case("A","devoice");
		Case  B = new Case("B","devoice");
		List<Judge> jl = new ArrayList<>();
		List<Case> cl  = new ArrayList<>();
		jl.add(a);
		jl.add(b);
		cl.add(A);
		cl.add(B);
		//法官a,b两人审理案件的数量必须相同
		CaseNumOfJudgeEqualRule rule = new CaseNumOfJudgeEqualRule(a,b);
		RuleSolver slv = new RuleSolver(cl,jl);
		slv.addRule(rule);
		slv.solve(2);
		HashMap<HashMap<Judge,List<Case>>,Integer> solution = slv.result();
		for (HashMap<Judge,List<Case>> res:solution.keySet()) {
			for (Judge j:res.keySet()) {
				System.out.print(j.name+"\t");
				for (Case c: res.get(j)) {
					System.out.print(c.name+",");
				}
				System.out.println();
			}
			System.out.println("---------------------");
		}
	}
}