package rules;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import cn.sjtu.lawcase.assignment.RuleSolver;
import cn.sjtu.lawcase.assignment.data.Case;
import cn.sjtu.lawcase.assignment.data.Judge;
import cn.sjtu.lawcase.assignment.rule.CaseNotToJudgeRule;

public class CasesNotAssignedToJudges{
	//添加限制条件：案件A,B不能由法官a审理，并对添加限制条件后的案件进行分案，并输出结果

	public static void main(String[] args) {
		Judge a = new Judge("a");
		Judge b = new Judge("b");
		Judge c = new Judge("c");
		Case  A = new Case("A","devoice");
		Case  B = new Case("B","devoice");
		Case  C = new Case("C","property");
		List<Judge> jl = new ArrayList<>();
		List<Case> cl  = new ArrayList<>();
		jl.add(a);
		jl.add(b);
		jl.add(c);
		cl.add(A);
		cl.add(B);
		cl.add(C);
		Vector<Case> cs = new Vector<>();
		cs.add(A);
		cs.add(B);
		//案件A,B不能由法官a审理
		CaseNotToJudgeRule rule = new CaseNotToJudgeRule(a,cs);
		RuleSolver slv = new RuleSolver(cl,jl);
		slv.addRule(rule);
		//计算2个可行解并输出可行解
		slv.solve(2);
		slv.printResults();
	}
}