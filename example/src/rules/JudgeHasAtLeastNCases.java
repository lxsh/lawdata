package rules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import cn.sjtu.lawcase.assignment.RuleSolver;
import cn.sjtu.lawcase.assignment.data.Case;
import cn.sjtu.lawcase.assignment.data.Judge;
import cn.sjtu.lawcase.assignment.rule.JudgeCaseNumberLimitRule;

public class JudgeHasAtLeastNCases{
	//添加限制条件：法官a必须审理超过n个案件，并对添加限制条件后的案件进行分案，并输出结果
	public void testJudge(Judge judge, List<Judge> judges, List<Case> cases, int n){
		//法官a必须审理超过n个案件
		JudgeCaseNumberLimitRule rule= new JudgeCaseNumberLimitRule(judge,n,"moreThan");
		RuleSolver slv = new RuleSolver(cases,judges);
		slv.addRule(rule);
		//计算可行解并返回可行解，根据返回的结果进行可阅读输出
		slv.solve(2);
		HashMap<HashMap<Judge,List<Case>>,Integer> solution = slv.result();
		for (HashMap<Judge,List<Case>> res:solution.keySet()) {
			for (Judge j:res.keySet()) {
				System.out.print(j.name+"\t");
				for (Case c: res.get(j)) {
					System.out.print(c.name+",");
				}
				System.out.println();
			}
			System.out.println("---------------------");
		}
		return;

}

	public static void main(String[] args) {
		Judge a = new Judge("a");
		Case  A = new Case("a","devoice");
		List<Judge> jl = new ArrayList<>();
		List<Case> cl  = new ArrayList<>();
		jl.add(a);
		cl.add(A);
        int n = 1;
        JudgeHasAtLeastNCases jhan = new JudgeHasAtLeastNCases();
		jhan.testJudge(a, jl,cl, 1);
	}

	
}