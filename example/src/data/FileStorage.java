package data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import cn.sjtu.lawcase.assignment.RuleSolver;
import cn.sjtu.lawcase.assignment.data.Case;
import cn.sjtu.lawcase.assignment.data.Judge;
import cn.sjtu.lawcase.assignment.rule.CaseToJudgeRule;
/*
 * 这个例子解释如何从文件读取法官和案件列表文件。并把分案结果存在另一个文件里。
 */
public class FileStorage {
	//在得到分案结果后，直接使用solver.dumpTo(filename)函数将结果储存在filename文件中
	public static void main(String[] args) {
		List<Case> c = new ArrayList<>();
		Random r = new Random(1);
		for(int i =0;i<100;++i) {
			String name = "案件+i";
			String type = "类别+r.nextInt(5)";
			Case a = new Case(name,type);
			c.add(a);
		}
		List<Judge> j = new ArrayList<>();
		for(int i=0;i<50;++i) {
			String name = "法官+i";
			Judge aa = new Judge(name);
			j.add(aa);
		}
		
		RuleSolver t = new RuleSolver(c,j);

		System.out.println("init done");
		for(int i=0;i<5;++i) {
			Random random = new Random();
			int number1 = random.nextInt(4);
			int number2 = random.nextInt(9);
			System.out.println(c.get(number2).name);
			CaseToJudgeRule con1 = new CaseToJudgeRule(j.get(number1),c.get(number2));
			System.out.println(con1.cases);
			t.addRule(con1);			
		}
		
		t.solve(1);
		try {
			t.dumpTo("output.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(r.size());

		return;
	}

}
